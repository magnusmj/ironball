/**
 * Renders the arena and everything on it.
 * @returns {none}
 */  
arena.render = () => {
  var team = arena.getTeam();
  if (team !== null && !arena.isNavigating() && arena.state === GameStates.Playing){
    if (team.indexOf(arena.ballHandler) === -1){      
      var currentDist = 10e10;
      if (arena.playerIndex !== -1){
        var player  = team[arena.playerIndex];
        if (!player.falling){
          currentDist = player.dist(arena.ballpos);
        }        
      }      
      smallestDist = 10e6;
      closestPlayerIndex = -1;
      for (var i = 0; i < 8; i++){
        var plr = team[i];
        var dist = plr.dist(arena.ballpos);

        if (dist<smallestDist && !plr.falling){
          smallestDist = dist;
          closestPlayerIndex=i;
        }
      }
      if (arena.playerIndex !== closestPlayerIndex && currentDist > smallestDist + 150){
        arena.playerIndex = closestPlayerIndex;
        var msg = { 
          t: MsgTypes.PlayerIsControlled, 
          pi: arena.playerIndex, 
          bk: k2b(arena.downKeys)
        }
        if (arena.ws.readyState == arena.ws.OPEN)
	        arena.ws.send(JSON.stringify(msg));
      }
    }
  }

  arena.updateUserInput();
  if (arena.crowdAudio.gain.gain.value < 0.5){
    arena.crowdAudio.gain.gain.value += 0.01;
  }
  ctx.fillStyle="grey";
  ctx.fillRect ( 0 , 0 , canvas.width , canvas.height );
  if (arena.arena !== null){
  	var viewTargetPos = null;
    if (arena.playerIndex !== -1){
    	var player = arena.getTeam()[arena.playerIndex];      
      if (player === undefined) return;
      else viewTargetPos = player.pos;
    } else {
      viewTargetPos = arena.ballpos;      
    }    
    camposdx = arena.camera.residualPos.x*arena.deltaTime/300;
    camposdy = arena.camera.residualPos.y*arena.deltaTime/300;
    arena.camera.residualPos.x -= camposdx;
    arena.camera.residualPos.y -= camposdy;
    arena.camera.pos.x += camposdx;
    arena.camera.pos.y += camposdy;
    var s = 2.5;
    var w = canvas.width*s;
    var h = w*1.8;
    var xlim = canvas.width*((s-1)/2);
    var ylim = h/2 - canvas.height/2;
    var cyoffset = canvas.height/6;
    
    var desiredCamPosX = Math.clamp(arena.ballpos.x, -xlim, xlim);
    var desiredCamPosY = Math.clamp(arena.ballpos.y-cyoffset, -ylim-canvas.width/2, ylim);
    arena.camera.residualPos.x = desiredCamPosX - arena.camera.pos.x;
    arena.camera.residualPos.y = desiredCamPosY - arena.camera.pos.y;
    var x = -w/2+canvas.width/2-arena.camera.pos.x;
    var y = -h/2+canvas.height/2+arena.camera.pos.y;

    var sc = arena.arena.width/canvas.width;
    ctx.drawImage(arena.arena, -x*sc/s, -y*sc/s, canvas.width*sc/s, canvas.height*sc/s, 0, 0, canvas.width, canvas.height);

		for (var i in arena.upgrades){
			var item = arena.upgrades[i];
			arena.renderItem(item, arena.camera.pos.x, arena.camera.pos.y);
		}

    arena.animPlayers(arena.camera.pos.x, arena.camera.pos.y);
    arena.renderBall(arena.camera.pos.x, arena.camera.pos.y);

    btnSize = Math.min(canvas.width, canvas.height)/2;
    if (arena.clientType !== ClientType.Spectator && !arena.handsOff){
      ctx.drawImage(arena.dir, 0, canvas.height-btnSize, btnSize, btnSize);
      ctx.drawImage(arena.btn, canvas.width -btnSize, canvas.height-btnSize, btnSize, btnSize);
    }    
    arena.renderTexts();   
    animations["getReady"].render();
	}
	if (arena.arenaMenuPos.x < 1){
		ctx.save();
    ctx.translate(arena.arenaMenuPos.x*canvas.width, 0);
		ctx.drawImage(arena.arenaMenu,0 , 0, canvas.width, canvas.height);
		if (arena.arenaMenuState != -1){
			ctrls = arena.arenaMenus[arena.arenaMenuState];
			for (var i = 0; i < ctrls.length; i++){
		    var ctrl = ctrls[i];
		    ctrl.render();
		  }
		}
		ctx.restore();
	}
	arena.arenaMenuPos.x += arena.arenaMenuSpeed.x;
	
	if (arena.arenaMenuPos.x < 0.5){
		arena.arenaMenuSpeed.x -= arena.arenaMenuPos.x/50; 
	} else {
		arena.arenaMenuSpeed.x += (1.1-arena.arenaMenuPos.x)/50; 
	}
	arena.arenaMenuSpeed.x *= 0.8;

	if (arena.playerIndex!=-1 && !arena.handsOff){
		var selectedPlayer = arena.getTeam()[arena.playerIndex];
		var selectItem = {
			pos: {
				x: selectedPlayer.pos.x/scale, 
				y: selectedPlayer.pos.y/scale+30
			}, 
			image: arena.selected, 
			size: { x: 20, y: 15 }
		}
		arena.renderItem(selectItem, arena.camera.pos.x, arena.camera.pos.y);
		if (arena.state === GameStates.Playing){
      arena.renderPointer(arena.camera.pos.x, arena.camera.pos.y, cyoffset);
    }
	}
	if (arena.state == GameStates.Goal){
		
		if (arena.lastBallHandler != null){
			console.log("render player that scored");
			var playerImage = arena.lastBallHandler.image;	
			if (playerImage != null) {
				ctx.drawImage(playerImage, 0, 0, canvas.width, canvas.width);
			}
			else {
				ctx.drawImage(playerImage, 0, 0, canvas.width, canvas.width);
			}
		} else console.log("no last ball handler!!");
		
	}
}

arena.renderPointer = (camposx, camposy, cyoffset) => {
  var team = arena.getTeam();
  if(team !== null){
    var player = team[arena.playerIndex];
    var dist = player.dist({x: camposx, y: camposy+cyoffset});
    if (dist/scale > 220){
      var dx = camposx - player.pos.x;
      var dy = (camposy+cyoffset) - player.pos.y;
      var angle = Math.atan2(dy, dx);
      ctx.fillStyle = "rgba(200,200,200,0.5)";
      ctx.save();
      ctx.translate(canvas.width/2, canvas.height/3);
      ctx.rotate(-angle+Math.PI);
      ctx.beginPath();
      ctx.moveTo(150, 0);
      ctx.lineTo(100, 50);
      ctx.lineTo(100, -50);
      ctx.closePath(); 
      ctx.fill();
      ctx.restore();
    }
  }
}

arena.renderTexts = () => {
  var xloc = 100*scale;
  var yloc = 30*scale;
  var fontsize = 25*scale;
  var teamColor = "#bb5";
  arena.renderText(arena.teamName1, {x: xloc, y: yloc}, fontsize, teamColor, "#333");
  arena.renderText(arena.score.team1, {x: xloc, y: yloc+fontsize}, fontsize, teamColor, "#333");
  xloc = canvas.width - xloc;
  teamColor = "#59c";
  arena.renderText(arena.teamName2, {x: xloc, y: yloc}, fontsize, teamColor, "#333");
  arena.renderText(arena.score.team2, {x: xloc, y: yloc+fontsize}, fontsize, teamColor, "#333");
  xloc = canvas.width/2;
  var time = roundTime - (arena.currentTimeStamp - arena.roundStartTime)/1000;
  arena.renderText(time.toFixed(0), {x: xloc, y: yloc+fontsize}, fontsize, "#bbb", "#333");
}

arena.renderText = (txt, loc, size = 30, fill = "#000", stroke = null, strokeWidth = scale) => {
  ctx.textAlign = "center";
  ctx.font = size + "px sans-serif";
  ctx.fillStyle = fill;
  ctx.fillText(txt,loc.x,loc.y);
  if (stroke !== null){
    ctx.lineWidth = strokeWidth;
    ctx.strokeStyle = stroke;
    ctx.strokeText(txt,loc.x,loc.y);
  }
}

arena.animPlayers = (camposx, camposy) => {
  var nonFallenPlayers = [];
  for (var i = 0; i < arena.team1.length; i++){
    var player = arena.team1[i];
    if (!player.falling){
      nonFallenPlayers.push(player);
    } else {
      arena.animPlayer(camposx, camposy, player);
    }
  }
  for (var i = 0; i < arena.team2.length; i++){
    var player = arena.team2[i];
    if (!player.falling){
      nonFallenPlayers.push(player);
    } else {
      arena.animPlayer(camposx, camposy, player);
    }
  }
  for (var i = 0; i < nonFallenPlayers.length; i++){
    var player = nonFallenPlayers[i];
    arena.animPlayer(camposx, camposy, player);
  }
}

arena.animPlayer = (camposx, camposy, player) => {
  var plrw = canvas.width/6;
  var plrh = plrw;
  var pxpos = player.pos.x + canvas.width/2 - camposx;
  var pypos = -player.pos.y + canvas.height/2 + camposy;
  ctx.save();
  ctx.translate(pxpos, pypos);
  ctx.rotate(-player.dir-Math.PI/2);
  ctx.translate(-plrw/2, -plrh/2);
  if (player.falling){
    var anim = player.team === 1 ? 'player1Fall' : 'player2Fall';
    var frame = animations[anim].frames[Math.min(4, player.animFrameIndex)];
    ctx.drawImage(arena.plrshadow, 0, 0, plrw, plrh);
    ctx.drawImage(frame, 0, 0, plrw, plrh);
  } else if (player.kicking){
    var anim = player.team === 1 ? 'player1Kick' : 'player2Kick';
    var frame = animations[anim].frames[Math.min(3, player.animFrameIndex)];
    ctx.drawImage(arena.plrshadow, 0, 0, plrw, plrh);
    ctx.drawImage(frame, 0, 0, plrw, plrh);
  } else if(player.throwing) {
    var anim = player.team === 1 ? 'player1Throw' : 'player2Throw';
    var frame = animations[anim].frames[Math.min(5, player.animFrameIndex)];
    ctx.drawImage(arena.plrshadow, 0, 0, plrw, plrh);
    ctx.drawImage(frame, 0, 0, plrw, plrh);
  } else if (player.running){
    var anim = player.team === 1 ? 'player1Run' : 'player2Run';
    var frame = animations[anim].frames[player.animFrameIndex%8];
    ctx.drawImage(arena.plrshadow, 0, 0, plrw, plrh);
    ctx.drawImage(frame, 0, 0, plrw, plrh);
  } else {
    ctx.drawImage(arena.plrshadow, 0, 0, plrw, plrh);
    ctx.drawImage(player.team === 1 ? arena.plr1 : arena.plr2, 0, 0, plrw, plrh);
  }
  if (player === arena.ballHandler){
    ctx.drawImage(arena.ballHandlerRing, 0, 0, plrw, plrh);
  }
  ctx.restore();
  ctx.save();
  ctx.translate(pxpos, pypos);
  ctx.beginPath();
  ctx.strokeStyle = '#333';
  ctx.lineWidth = 4;
  ctx.moveTo(-27*scale,50*scale);
  ctx.lineTo(27*scale,50*scale);
  ctx.stroke();
  ctx.beginPath();
  ctx.lineWidth = 2;
  ctx.strokeStyle = '#b55';
  ctx.moveTo(-25*scale,50*scale);
  ctx.lineTo((25)*scale, 50*scale);
  ctx.stroke();
  ctx.beginPath();
  ctx.strokeStyle = '#5b5';
  ctx.lineWidth = 2;
  ctx.moveTo(-25*scale,50*scale);
  ctx.lineTo((-25+player.health/2)*scale, 50*scale);    
  ctx.stroke();
  ctx.restore();
}

arena.renderItem = (item, camposx, camposy) => {
  var pxpos = item.pos.x*scale + canvas.width/2 - camposx -item.size.y*scale/2;
  var pypos = -item.pos.y*scale + canvas.height/2 + camposy - item.size.y*scale;
  ctx.drawImage(item.image, pxpos, pypos, item.size.x*scale, item.size.y*scale);
}

arena.renderBall = (camposx, camposy) => {
  if (arena.ballHandler === null){
    var ballsz = canvas.width/50*(1+arena.ballpos.z*0.5); 
    var pxpos = arena.ballpos.x + canvas.width/2 - camposx - ballsz;
    var pypos = -arena.ballpos.y + canvas.height/2 + camposy - ballsz;
    ctx.drawImage(arena.plrshadow, pxpos, pypos, ballsz*2, ballsz*2);
    var pxpos = arena.ballpos.x + canvas.width/2 - camposx - ballsz/2;
    var pypos = -arena.ballpos.y + canvas.height/2 + camposy - ballsz/2;
    ctx.drawImage(arena.ball, pxpos, pypos, ballsz, ballsz);
  }
}
