
ArenaMenuStates = {
  SinglePlayerStartMenu: 0,
  InGameMenu: 1,
  TacticsMenu: 2,
  FormationsMenu: 3,
  WaitForOpponentMenu: 4
}

arena.isNavigating = () => {
  var isNavigating = false;
  if (arena.downKeys['a']) isNavigating = true;
  if (arena.downKeys['s']) isNavigating = true;
  if (arena.downKeys['d']) isNavigating = true;
  if (arena.downKeys['w']) isNavigating = true;
  return isNavigating;
}

arena.getTeam = () => {
  if (arena.clientType === ClientType.Team1){
    return arena.team1;
  } else if(arena.clientType === ClientType.Team2){
    return arena.team2;
  }
  return null;
}

arena.setRenderer = () => {
	arena.initLogic();
  history.pushState({state: "gameRenderer"}, "game", "index.html");
  arena.playAgainstAI = false;
  arena.playerIndex = -1;
  arena.ballHandler = null;
  if (currentRenderer !== undefined){
    currentRenderer.unsetRenderer();
  }
  arena.pickupAudio.play();
  arena.ballAudio.play();
  canvas.addEventListener("touchstart", arena.touchStart, {passive: false});
  canvas.addEventListener("touchmove", arena.touchMove, {passive: false});
  canvas.addEventListener("touchend", arena.touchEnd, {passive: false});
  window.addEventListener("keydown", arena.keyDown, {passive: false});
  window.addEventListener("keyup", arena.keyUp, {passive: false});  
  canvas.addEventListener("mousedown", arena.touchStart, false); 
  canvas.addEventListener("mouseup", arena.touchEnd, false);   
  canvas.addEventListener("mousemove", arena.touchMove, false);  
  
  arena.crowdAudio.volume = 0;
  arena.crowdAudio.play(0);
  arena.beepAudio.play(0);  
  currentRenderer = arena;
  
  var protocol = location.protocol === "https:" ? "wss://" : "ws://";
  arena.ws = new WebSocket(protocol + location.host);  
  arena.ws.onopen = function()
  {
    var msg = {
      t: MsgTypes.ArenaConnection, 
      tm: JSON.stringify(team),
      arenaID: arena.arenaID,
      handsOff: arena.handsOff,
      teamId: menuRenderer.tournament.teamId
    }
    arena.ws.send(JSON.stringify(msg));
    console.log("Connection request is sent...");
    arena.connected = true;
  }
  arena.ws.onmessage = arenaMessageHandler;
  arena.ws.onclose = function()
  {
    console.log("Connection is closed...");
    arena.crowdAudio.stop();
    menuRenderer.setRenderer();
  }
  window.onbeforeunload = function(event) {
    arena.ws.close();
  }
}

arena.attachPlayerImages = () => {
	var team1Images = null;
	var team2Images = null;
	if (arena.team1Id == menuRenderer.tournament.teamId)
		 team1Images = playerImages;
	else team1Images = menuRenderer.tournament.participants[arena.team1Id].playerImages;
	if (arena.team2Id == menuRenderer.tournament.teamId)
		 team2Images = playerImages;
	else team2Images = menuRenderer.tournament.participants[arena.team2Id].playerImages;
	for (var i = 0; i < playerCount; i++){
		if (team1Images != null) arena.team1[i].image = team1Images[i];
		else arena.team1[i].image = defaultPlayerImage;
		if (team2Images != null) arena.team2[i].image = team2Images[i];
		else arena.team2[i].image = defaultPlayerImage;		
	}	
}

arena.getUpgrade = (id) => {
	for (i in arena.upgrades){
		var upgrade = arena.upgrades[i];
		if (upgrade.id == id) return upgrade;
	}
	return null;
}

arena.removeUpgrade = (id) => {
	for (i in arena.upgrades){
		var upgrade = arena.upgrades[i];
		if (upgrade.id == id){
			arena.upgrades.splice(i, 1);
			return;
		}
	}
}

arena.unsetRenderer = () => {
  canvas.removeEventListener("touchstart", arena.touchStart);
  canvas.removeEventListener("touchmove", arena.touchMove);
  canvas.removeEventListener("touchend", arena.touchEnd);
  window.removeEventListener("keydown", arena.keyDown);
  window.removeEventListener("keyup", arena.keyUp);  
  canvas.removeEventListener("mousedown", arena.touchStart); 
  canvas.removeEventListener("mouseup", arena.touchEnd);   
  canvas.removeEventListener("mousemove", arena.touchMove);    
  currentRenderer = null;
  arena.ws.close();
}

arena.setState = (state) => {
  arena.state = state;
  switch (state) {
    case GameStates.Paused:        
      break;
    case GameStates.Playing:
      if (isClient){
        arena.goAudio.play(0);
      }
      break;
    case GameStates.Goal:
      break;
    case GameStates.HalfTime:
      break;
    case GameStates.GetReady:
      if (isClient){
        arena.getReadyAudio.play(0);
        animations['getReady'].start();
        arena.playerIndex = -1;          
      }
      break;
    case GameStates.Finished:
      if (isClient){
        arena.beepAudio.play(0);          
      }
      break;
  }
}

