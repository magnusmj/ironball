/**
 * onmessage recieves messages from the server to interpret.
 * @param {object} evt the event from the server contains a type and metadata about the event.
 * @returns {none}
 */  
arenaMessageHandler = function (evt)
{    
  var msg = JSON.parse(evt.data);    
  switch (msg.t){
    case MsgTypes.PlayerReturn:
      var player = null;
      if (msg.tm === 1){
        player = arena.team1[msg.pi];
      } else {
        player = arena.team2[msg.pi];
      }
      player.returning = true;
      player.posResidual.x = msg.pos.x*scale - player.pos.x;
      player.posResidual.y = msg.pos.y*scale - player.pos.y;
      player.dir = msg.dir;
      arena.updatePlayerInput(player, msg.bk);
      break;
    case MsgTypes.PlayerMelee:
      var player = null;
      if (msg.tm === 1){
        player = arena.team1[msg.pi];
      } else {
        player = arena.team2[msg.pi];
      }
      var otherPlayer = null;
      if (msg.op.tm === 1){
        otherPlayer = arena.team1[msg.op.pi];
      } else {
        otherPlayer = arena.team2[msg.op.pi];
      }
      player.kicking = false;
      otherPlayer.falling = true;
      otherPlayer.animFrameIndex = 0;
      otherPlayer.animFrameTime = arena.currentTimeStamp;
      otherPlayer.posResidual.x = msg.op.pos.x*scale - otherPlayer.pos.x;
      otherPlayer.posResidual.y = msg.op.pos.y*scale - otherPlayer.pos.y;

      if (isClient){
        player.kickImpactAudio.play();
        player.fallAudio.play();
      }
      break;
    case MsgTypes.PlayerInputUpdate:
      var player = null;
      if (msg.tm === 1){
        player = arena.team1[msg.pi];
      } else {
        player = arena.team2[msg.pi];
      }
      player.posResidual.x = (msg.pos.x*scale - player.pos.x);
      player.posResidual.y = (msg.pos.y*scale - player.pos.y);
      player.dir = msg.dir;
      if (msg.tdir){
        player.targetDir = msg.tdir;
        player.speed = 0;          
      }
      arena.updatePlayerInput(player, msg.bk);        
      break;
    case MsgTypes.PlayerPositionSync:
      var player = null;
      if (msg.tm === 1){
        player = arena.team1[msg.pi];
      } else {
        player = arena.team2[msg.pi];
      }
      player.posResidual.x = msg.pos.x*scale - player.pos.x;
      player.posResidual.y = msg.pos.y*scale - player.pos.y;
      break;
    case MsgTypes.PlayerHealthChange:
      var player = null;
      if (msg.tm === 1){
        player = arena.team1[msg.pi];
      } else {
        player = arena.team2[msg.pi];
      }
      player.health = msg.value;
      break;
    case MsgTypes.BallHandlerChanged:
      var player = null;
      if (msg.tm === 1){
        player = arena.team1[msg.pi];
      } else {
        player = arena.team2[msg.pi];
      }
      player.posResidual.x = msg.pos.x*scale - player.pos.x;
      player.posResidual.y = msg.pos.y*scale - player.pos.y;
      if (arena.ballHandler != null)
      	arena.lastBallHandler = arena.ballHandler;
      arena.ballHandler = player;
      arena.pickupAudio.play();
      if (msg.tm === arena.clientType && arena.playerIndex !== msg.pi){
        if (arena.playerIndex !== -1){
          var oldPlayer = arena.getTeam()[arena.playerIndex];
          oldPlayer.running = false;   
        }         
        arena.playerIndex = msg.pi;
        var msg = { 
          t: MsgTypes.PlayerIsControlled, 
          pi: arena.playerIndex, 
          bk: k2b(arena.downKeys)
        };
        arena.ws.send(JSON.stringify(msg));
      }
      break;
    case MsgTypes.BallSync:
      arena.ballposResidual.x = msg.pos.x*scale - arena.ballpos.x;
      arena.ballposResidual.y = msg.pos.y*scale - arena.ballpos.y;
      arena.ballSpeed = msg.spd;
      break;
    case MsgTypes.ScoreUpdate:
      arena.score = msg.score;
      arena.crowdCheerAudio.play();
      arena.beepAudio.play();
      arena.scoreAudio.play();
      if (msg.restart){                              
        arena.restartGame();
      }
      break;
    case MsgTypes.RoundEnded:
      arena.roundStartTime = arena.currentTimeStamp;
      arena.switchSide();
      arena.restartGame();
      arena.getReadyAudio.play();
      this.roundStartTime = this.currentTimeStamp;
      arena.round += 1;
      break;
    case MsgTypes.ChangeGameState:
    	if (arena.state == GameStates.PreGame && msg.state == GameStates.GetReady){
    		arena.arenaMenuState = ArenaMenuStates.InGameMenu;
    		arena.arenaMenuSpeed.x = 0.3;
    	}
    	if (msg.state === GameStates.Finished){
    		console.log("GameStates.Finished")
        menuRenderer.state = MenuStates.GameOverMenu;
        menuRenderer.renderScreen = false;
        if (arena.score.team1 === arena.score.team2){
        	localStorage.winner = "Tie";
        } else {
        	localStorage.winner = arena.score.team1 > arena.score.team2 ? arena.teamName1 : arena.teamName2;
        }
        history.back();
      }
      arena.setState(msg.state);
      break;
    case MsgTypes.BallThrown:      
      var bh = arena.ballHandler;
      arena.ballSpeed = msg.spd;
      arena.ballpos.x = bh.pos.x;   
      arena.ballpos.y = bh.pos.y;   
      arena.ballpos.z = msg.bp.z;     
      if (arena.ballHandler != null)
 	    	arena.lastBallHandler = arena.ballHandler;
      arena.ballHandler = null;
      arena.ballposResidual = {
      	x: msg.bp.x*scale - bh.pos.x, 
      	y: msg.bp.y*scale - bh.pos.y, 
      	z: 0
      };
      break;
    case MsgTypes.SyncTeamNames:
      arena.teamName1 = msg.tn1;
      arena.teamName2 = msg.tn2;     
      arena.team1Id = msg.id1;
      arena.team2Id = msg.id2;  
      arena.attachPlayerImages();
      break;
    case MsgTypes.Connected:
    	console.log("connected");
      arena.clientType = msg.ct;
      arena.ballpos = msg.ballpos;
      arena.currentTimeStamp = Date.now();
      arena.lastTimeStamp = arena.currentTimeStamp;         
      arena.deltaTime = 0;
      arena.roundStartTime = arena.lastTimeStamp + msg.roundStartTime;
      arena.teamName1 = msg.teamName1;
      arena.teamName2 = msg.teamName2;
      arena.round = msg.round;
      arena.score = msg.score;
      arena.state = msg.state;
      arena.playerIndex = -1;
      arena.upgrades = [];
      arena.handsOff = msg.handsOff;
      for (var i in msg.upgrades){
      	var upgrade = msg.upgrades[i];
      	upgrade.image = upgradeImages[upgrade.type];
      	arena.upgrades.push(upgrade);
      }
      for (var i = 0; i < msg.team1.length; i++){
        arena.team1[i].sync(msg.team1[i]);
        arena.team2[i].sync(msg.team2[i]);
      }
      if (arena.state != GameStates.PreGame) {
      	arena.arenaMenuState = ArenaMenuStates.InGameMenu;
				arena.arenaMenuSpeed.x = 0.3;
      } else {
      	arena.arenaMenuState = ArenaMenuStates.WaitForOpponentMenu;
				arena.arenaMenuSpeed.x = -0.3;
				arena.arenaMenus[ArenaMenuStates.SinglePlayerStartMenu][0].value = arena.arenaID.toString();
      }
      break;
    case MsgTypes.UpgradeTaken:
    	arena.removeUpgrade(msg.upg.id);
    	upgradeSounds[msg.upg.type].play();
    	if (msg.upg.type == UpgradeTypes.HealthUpgrade){
    		var player = msg.tm === 1 ?  arena.team1[msg.pi] : arena.team2[msg.pi];		     
	      player.health = Math.min(100, player.health + 20);
    	}
    	break;
    case MsgTypes.SpawnUpgrade:
    	var upgrade = msg.upg;
    	upgrade.image = upgradeImages[upgrade.type];
    	arena.upgrades.push(upgrade);
    	break;
    default:
      console.log(evt.data);
      var type = Object.keys(MsgTypes).find(key => MsgTypes[key] === msg.t)
      console.log("type: " + type);
  }
}
