
arena.touchStart = (e)=>{
  e.preventDefault();
  var touch = null;
  arena.touching = true;
  if(e.touches == undefined){
    for (var i = 0; i< menuRenderer.ctrls().length; i++){
      touch = e;
      e.touches = [];
    }     
  } else {
	  touch = e.touches[e.which];  	  
	}
  arena.lastTouch.x = touch.clientX;
  arena.lastTouch.y = touch.clientY;
  for (var i = 0; i < e.touches.length; i++){
    arena.updateMoveTouch(e.touches[i], false);
  }  
  if (arena.arenaMenuPos.x<1){
		ctrls = arena.arenaMenus[arena.arenaMenuState];
		for (var i = 0; i < ctrls.length; i++){
		  var ctrl = ctrls[i];
			if (ctrl.inside({x: touch.clientX, y: touch.clientY})){
				if (ctrl.pressed === false){
					ctrl.pressed = true;
					ctrl.pressAudio.play();
					if (navigator.vibrate){            
					  navigator.vibrate(25);            
					}
				}
			} else {
				if (ctrl.pressed === true){
					ctrl.pressed = false;
					ctrl.releaseAudio.play();
				}
			}
		}
  }
}

arena.touchMove = (e)=>{
  e.preventDefault();
  var touch = null;
  if(e.touches == undefined){
    for (var i = 0; i< menuRenderer.ctrls().length; i++){
      touch = e;
      e.touches = [];
    }     
  } else {
	  touch = e.touches[e.which];  	  
	}
  var dx = touch.clientX-arena.lastTouch.x;
  var dy = touch.clientY-arena.lastTouch.y;
  arena.lastTouch.x = touch.clientX;
  arena.lastTouch.y = touch.clientY;  
  arena.updateMoveTouch(touch, false, true);
}

arena.touchEnd = (e)=>{
  e.preventDefault();
	
  var touch = null;
  if(e.touches == undefined){
    for (var i = 0; i< menuRenderer.ctrls().length; i++){
      touch = e;
      e.touches = [];
    }     
  } else {
	  touch = e.changedTouches[e.which];  	  
	}
	if (e.touches.length==0) arena.touching = false;
  arena.updateMoveTouch(touch, true);
  if (arena.arenaMenuPos.x<1){
  ctrls = arena.arenaMenus[arena.arenaMenuState];
		for (var i = 0; i < ctrls.length; i++){
			var ctrl = ctrls[i];
			if (ctrl.pressed === true){
				ctrl.pressed = false;
				ctrl.releaseAudio.play();
				ctrl.clicked(ctrl);
			}
		}
  }
}

arena.keyDown = (e)=>{
  e.preventDefault();
  //console.log("'" + e.key + "'");
  arena.downKeys[e.key] = true;  
	arena.handleDebugInput(e);
}

arena.handleDebugInput = (e) => {
  if (e.key == "å"){
  	var msg = {
		  t: MsgTypes.EndRoundDebug,
		}
		arena.ws.send(JSON.stringify(msg));
  }
  if (e.key == "æ"){
  	var msg = {
		  t: MsgTypes.Team1ScoreDebug,
		}
		arena.ws.send(JSON.stringify(msg));
  }
  if (e.key == "ø"){
  	var msg = {
		  t: MsgTypes.Team2ScoreDebug,
		}
		arena.ws.send(JSON.stringify(msg));
  }
}

arena.keyUp = (e)=>{
  arena.downKeys[e.key] = false;
}

arena.updateMoveTouch = (touch, release, slide = false) => {
	if (touch == undefined) {
		arena.downKeys['a'] = false;
    arena.downKeys['s'] = false;
    arena.downKeys['d'] = false;
    arena.downKeys['w'] = false;
    arena.downKeys[' '] = false;
    return;
	}
	inputFieldOffset = 2*canvas.width/3
	if (arena.handsOff)
	{
		inputFieldOffset = 0
	}
	
  if (touch.clientY>canvas.height-inputFieldOffset){
    if(touch.clientX<canvas.width/2){
      arena.downKeys['a'] = false;
      arena.downKeys['s'] = false;
      arena.downKeys['d'] = false;
      arena.downKeys['w'] = false;
      if (release){

      } else {        
        if (touch.clientY > canvas.height-2*canvas.width/10)
          arena.downKeys['s'] = true;
        if (touch.clientY < canvas.height-3*canvas.width/10)
          arena.downKeys['w'] = true;
        if (touch.clientX > 3*canvas.width/10)
          arena.downKeys['d'] = true;
        if (touch.clientX < 2*canvas.width/10)
          arena.downKeys['a'] = true;
      }
    } else {
      arena.downKeys[' '] = false;
      if (release || slide){

      } else {
        arena.downKeys[' '] = true;
        if (navigator.vibrate){
          navigator.vibrate(25);
        }
      }
    }
  } else {  	
	  var loc = {x: touch.clientX, y: touch.clientY};
	  var delta = {x: loc.x-arena.lastLoc.x, y: loc.y-arena.lastLoc.y};    
	  arena.lastLoc = loc;
	  if (slide && arena.touching){
	  	arena.arenaMenuSpeed.x += delta.x/canvas.width;
  	}
  }
}

arena.updateUserInput = () => {
  if (arena.connected){
    var newInput = k2b(arena.downKeys); 
    if (newInput !== arena.lastInput){
      arena.ws.send(JSON.stringify({t: MsgTypes.UserInputUpdate, pi: arena.playerIndex,  bk: newInput}));
      arena.lastInput = newInput;
    }
  }
}

