var ClientType = {
  Team1: 1,
  Team2: 2,
  Spectator: 0
};

var upgradeImages = [
	GetImage("img/items/coins.png"),
	GetImage("img/items/speedupgrade.png"), 
	GetImage("img/items/throwupgrade.png"),
	GetImage("img/items/stamminaupgrade.png"),
	GetImage("img/items/accelerationupgrade.png"),
	GetImage("img/items/kickupgrade.png"),
	GetImage("img/items/intelligenceupgrade.png"),
	GetImage("img/items/enduranceupgrade.png"),
	GetImage("img/items/medkit.png")
];

var upgradeSounds = [];
var animations = {};
animations['player1Run'] = new Animator([
  "img/player1/sm_run1.png",
  "img/player1/sm_run2.png",
  "img/player1/sm_run3.png",
  "img/player1/sm_run4.png",
  "img/player1/sm_run5.png",
  "img/player1/sm_run6.png",
  "img/player1/sm_run7.png",
  "img/player1/sm_run8.png"]
);

animations['player1Throw'] = new Animator([
  "img/player1/sm_throw1.png",
  "img/player1/sm_throw2.png",
  "img/player1/sm_throw3.png",
  "img/player1/sm_throw4.png",
  "img/player1/sm_throw5.png",
  "img/player1/sm_throw6.png"]
);

animations['player1Fall'] = new Animator([
  "img/player1/sm_idle.png",
  "img/player1/sm_fall1.png",
  "img/player1/sm_fall2.png",
  "img/player1/sm_fall3.png",
  "img/player1/sm_fall4.png"]
);
animations['player1Kick'] = new Animator([
  "img/player1/sm_idle.png",
  "img/player1/sm_run1.png",
  "img/player1/sm_run2.png",
  "img/player1/sm_run3.png"]
);

animations['player2Run'] = new Animator([
  "img/player2/sm_run1.png",
  "img/player2/sm_run2.png",
  "img/player2/sm_run3.png",
  "img/player2/sm_run4.png",
  "img/player2/sm_run5.png",
  "img/player2/sm_run6.png",
  "img/player2/sm_run7.png",
  "img/player2/sm_run8.png"]
);
animations['player2Throw'] = new Animator([
  "img/player2/sm_throw1.png",
  "img/player2/sm_throw2.png",
  "img/player2/sm_throw3.png",
  "img/player2/sm_throw4.png",
  "img/player2/sm_throw5.png",
  "img/player2/sm_throw6.png"]
);
animations['player2Fall'] = new Animator([
  "img/player2/sm_idle.png",
  "img/player2/sm_fall1.png",
  "img/player2/sm_fall2.png",
  "img/player2/sm_fall3.png",
  "img/player2/sm_fall4.png"]
);
animations['player2Kick'] = new Animator([
  "img/player2/sm_idle.png",
  "img/player2/sm_run1.png",
  "img/player2/sm_run2.png",
  "img/player2/sm_run3.png"]
);

animations['getReady'] = new Animator([
  "img/getready/0000.png",
  "img/getready/0001.png",
  "img/getready/0002.png",
  "img/getready/0003.png",
  "img/getready/0004.png",
  "img/getready/0005.png",
  "img/getready/0006.png",
  "img/getready/0007.png",
  "img/getready/0008.png",
  "img/getready/0009.png",
  "img/getready/0010.png",
  "img/getready/0011.png",
  "img/getready/0012.png",
  "img/getready/0013.png"]
);
animations['getReady'].fadeOut = true;

var defaultPlayerImage = new Image()
defaultPlayerImage.src = "img/default.png";

var arena = new GameLogics();

arena.init = () => {
  arena.initLogic();
  arena.initAV();

}

arena.initAV = () => {
	arena.clientType = ClientType.Spectator;
	arena.playerIndex = -1;
	arena.camera = {pos: {x: 0, y: 0}, residualPos: {x: 0, y: 0}};
	arena.arenaMenuPos = {x: 1, y: 0};
	arena.arenaMenuSpeed = {x: 0, y: 0};
	arena.arenaMenuState = 0;
	arena.handsOff = false;
	arena.lastLoc = {x:0, y:0};
	arena.downKeys = b2k(0);
	arena.lastInput = null;
	arena.connected = false;
	arena.arena = new Image();
	arena.arena.src = 'img/sm_ArenaFull.jpg';
	arena.arenaMenu = new Image();
	arena.arenaMenu.src = 'img/sm_arenaMenu.png';
	arena.dir = new Image();
	arena.dir.src = 'img/dir.png';
	arena.selected = new Image();
	arena.selected.src = 'img/selected.png';
	arena.btn = new Image();
	arena.btn.src = 'img/btn.png';
	arena.plr1 = new Image();
	arena.plr1.src = 'img/player1/sm_idle.png';
	arena.plr2 = new Image();
	arena.plr2.src = 'img/player2/sm_idle.png';
	arena.ballHandlerRing = new Image();
	arena.ballHandlerRing.src = 'img/ballHandler.png';
	arena.plrshadow = new Image();
	arena.plrshadow.src = 'img/sm_shadow.png';
	arena.ball = new Image();
	arena.ball.src = 'img/sm_ironball.png';
	arena.crowdCheerAudio = new GameAudio("snd/crowdCheer.wav", false);
	arena.ballAudio = new GameAudio("snd/ball.wav", false);
	arena.pickupAudio = new GameAudio("snd/pickup.wav", false);  
	arena.whoshAudio = new GameAudio("snd/whosh.wav", false);
	arena.crowdAudio = new GameAudio("snd/crowd.wav", true);
	arena.beepAudio = new GameAudio("snd/beep.wav", false);
	arena.scoreAudio = new GameAudio("snd/score.wav", false, 1);
	arena.getReadyAudio = new GameAudio("snd/getReady.wav", false);
	arena.goAudio = new GameAudio("snd/go.wav", false);
	animations['getReady'].size.x = canvas.width;
	animations['getReady'].size.y = canvas.width;
	arena.arenaID = 0;
	arena.arenaMenus = {}

	if (upgradeSounds.length == 0){
		upgradeSounds = [
			new GameAudio("snd/coinsPickup.wav", false),
			new GameAudio("snd/pickupUpgrade.wav", false, 0.5), 
			new GameAudio("snd/pickupUpgrade.wav", false, 0.5),
			new GameAudio("snd/pickupUpgrade.wav", false, 0.5),
			new GameAudio("snd/pickupUpgrade.wav", false, 0.5),
			new GameAudio("snd/pickupUpgrade.wav", false, 0.5),
			new GameAudio("snd/pickupUpgrade.wav", false, 0.5),
			new GameAudio("snd/pickupUpgrade.wav", false, 0.5),
			new GameAudio("snd/medkitPickup.wav", false, 0.5)
		];
	}
	var ctrls = [];
	arena.arenaMenus[ArenaMenuStates.SinglePlayerStartMenu] = ctrls;
	ctrls.push(new TextBox('img/txtbx.png', {x: 0.125, y: 0.18}, Control.Sizes["Wide"], "Invite friend to arena ID:", arena.arenaID.toString(), 32));
	ctrls.push(new Button('img/wbtn.png', 'img/wbtnpress.png', {x: 0.125, y: 0.35}, Control.Sizes["Wide"], "Play against AI", 30, "dark"));
	ctrls[0].editable = false;
	ctrls[1].clicked = () => {
		arena.playAgainstAI = true;
		arena.ws.send(JSON.stringify({t: MsgTypes.ArenaPlayAgainstAI}));    
		arena.arenaMenuSpeed.x = 0.3;
		arena.arenaMenuState = ArenaMenuStates.InGameMenu;
	}

	var ctrls = [];
	arena.arenaMenus[ArenaMenuStates.InGameMenu] = ctrls;
	ctrls.push(new Button('img/sm_wbtn.png', 'img/sm_wbtnpress.png', {x: 0.125, y: 0.07}, Control.Sizes["Wide"], "Formations", 30, "dark"));
	ctrls.push(new Button('img/sm_wbtn.png', 'img/sm_wbtnpress.png', {x: 0.125, y: 0.20}, Control.Sizes["Wide"], "Tactics", 30, "dark"));
	ctrls[0].clicked = () => {    
		arena.arenaMenuState = ArenaMenuStates.FormationsMenu;
	}

	var ctrls = [];
	arena.arenaMenus[ArenaMenuStates.WaitForOpponentMenu] = ctrls;
	ctrls.push(new TextBox('img/txtbx.png', {x: 0.125, y: 0.18}, Control.Sizes["Wide"], "Wait for:", "Opponent", 32));
	ctrls[0].editable = false;
	arena.initFormationsMenu(); 
}

arena.initFormationsMenu = () => {
	var ctrls = [];
  arena.arenaMenus[ArenaMenuStates.FormationsMenu] = ctrls;  
  ctrls.push(new Button('img/sm_nbtn.png', 'img/sm_nbtnpress.png', {x: 0.125, y: 0.815}, Control.Sizes["Narrow"], "Back", 30, "dark"));
  ctrls[0].clicked = () => {    
  	arena.arenaMenuState = ArenaMenuStates.InGameMenu;
  }
  var counter = 0;
  for (formationsIndex in team.formations){
		var formation = team.formations[formationsIndex];
		var positionsName = formation.name;
		var pos = formation.positions;
		var btn = new Button('img/sm_wbtn.png', 'img/sm_wbtnpress.png', {x: 0.125, y: 0.065+counter*0.125}, Control.Sizes["Wide"], positionsName, 30, "dark"); 
		btn.formationIndex = formationsIndex;
		btn.clicked = (sender) => {
			var msg = { 
				t: MsgTypes.ChangeFormation, 
				frm: team.formations[sender.formationIndex].positions // defaultPositions[sender.text]        
			}
			arena.ws.send(JSON.stringify(msg));
			arena.arenaMenuSpeed.x = 0.3;
		}
		ctrls.push(btn);
		counter++;
  }
}

