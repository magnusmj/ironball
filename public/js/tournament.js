
var Tournament = function(){
  this.connected = false;
  this.ws = null;
  this.teamId = 0;
  this.teamTournamentState = null;
  this.playerUpgrades = {};
  this.gameType = -1;
  this.kachingAudio = new GameAudio("snd/kaching.wav", false);
  this.tree = null;
  this.participants = {};
  
  this.onmessage = (evt) => {
    var msg = JSON.parse(evt.data);
    //console.log(evt.data);
    switch (msg.t){
      case MsgTypes.Connected:
        localStorage.setItem("tournamentID", msg.id);
        this.teamId = msg.teamId;
        this.teamTournamentState = msg.teamTournamentState;
        this.gameType = msg.gt;
        //console.log(evt.data);
        menuRenderer.menus[MenuStates.TeamManagerMenu][2].updateText();
        break;
      case MsgTypes.TournamentStateChanged:
        localStorage.setItem("playerCount", msg.playerCount);
        localStorage.setItem("poolSize", msg.poolSize);
        localStorage.setItem("aiTeamCount", msg.aiTeamCount);
        localStorage.setItem("poolCount", msg.poolCount);
        
        ctrls = menuRenderer.menus[MenuStates.NewTournamentMenu];
        ctrls[7].switchValue = msg.handsOff;
        
        this.tree = msg.tt;
        for (var i in msg.p){
        	var participant = msg.p[i];
        	if (participant.id in this.participants){
        		this.participants[participant.id].score = participant.score;
        		this.participants[participant.id].wins = participant.wins;
        	}else this.participants[participant.id] = participant;
        }
        if (msg.gt === GameTypes.SingleMatch){
        	if (menuRenderer.state == MenuStates.InvitePlayerMenu && msg.playerCount >= 2){
				  	menuRenderer.state = MenuStates.TeamManagerMenu;
				  	menuRenderer.renderScreen = true;        	
        	}        	
        }
        if (this.tree.gamesFinished.length != 0 && this.tree.gamesInProgress.length == 0 && this.tree.gamesUnfinished.length == 0){
		  		menuRenderer.state = MenuStates.TournamentFinishedMenu;
		  		var winner = null;
		  		for (var id in this.participants){
		  			var participant = this.participants[id];
		  			if (winner == null) winner = participant;
		  			else {
		  				if (winner.wins < participant.wins) winner = participant;
		  				else if (winner.wins == participant.wins && winner.score < participant.score) winner = participant;
		  			}
		  		}
					menuRenderer.menus[MenuStates.TournamentFinishedMenu][1].value = winner.name;
					menuRenderer.renderScreen = false;        	
		  	}
        break;
      case MsgTypes.TeamTournamentStateChanged:
      	//console.log(evt.data);
      	if (this.teamTournamentState.id === msg.teamTournamentState.id){
      		this.teamTournamentState = msg.teamTournamentState;
      		menuRenderer.menus[MenuStates.TeamManagerMenu][2].updateText();
      		this.kachingAudio.play();
      	}      	
      	break;
      case MsgTypes.TeamIdChanged:
      	
      	break;
      case MsgTypes.ArenaCreated:
      	arena.arenaID = msg.id;
        arena.handsOff = false;
        arena.setRenderer();        
      	break;
      case MsgTypes.TeamAdded:
      	var newTeamId = msg.id;
      	var participant = msg.tm;
      	participant.playerImages = [];
      	this.participants[newTeamId] = participant;
      	this.getTeamImages(newTeamId);
      	break;
			case MsgTypes.StartTournament:
				menuRenderer.state = MenuStates.TeamManagerMenu;
				menuRenderer.menus[MenuStates.TeamManagerMenu][2].updateText();
				menuRenderer.renderScreen = true;
				break;
      default:
      	console.log(evt.data);
    }
  }
  this.onclose = () => {
    console.log("Connection to tournament is closed...");   
    menuRenderer.state = MenuStates.MainMenu; 
  	menuRenderer.renderScreen = false;
    this.ws = null;
  }
  this.onDoneClicked = () => {
  	var msg = {
  		t: MsgTypes.TeamManagementDone
  	}
  	var data = JSON.stringify(msg);
  	this.ws.send(data);
  }
  this.getTeamImages = (id) => {
  	
  	var participant = this.participants[id];
  	console.log("getting images from team: " + participant.name)
  	for (var i = 0; i < playerCount; i++){
			var pl = participant.players[i];
			var img = new Image();
			if (pl.imageData!=null){
				console.log("image was had data")
				img.addEventListener('load', playerImageLoaded);
				img.src = pl.imageData;				
			} else {
				console.log("image was null")
				img.src = "img/default.png";	
			}
			participant.playerImages.push(img);
		}
  }
}


