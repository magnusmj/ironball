var LobbyScreen = function (surfaceimg, loc, size){    
  Screen.call(this, surfaceimg, loc, size);
  this.pools = [];
  this.avatars = [];
  this.currentAvatar = null;
  this.currentViewIndex = 0;
  this.moveDistance = 0;
  this.moveSpeed = 0;
  this.pools.push(new Pool(2, {x: 0.01, y:0.01}));
  this.pools.push({loc: {x: 0.51, y:0.01}, size: {x: 0.48, y: 0.3}});
  this.render = () => {
    var size = { x: this.size.x * canvas.width, y: this.size.y * canvas.height };
    var loc = { x: this.loc.x * canvas.width, y: this.loc.y * canvas.height };
    ctx.drawImage(this.surface, loc.x, loc.y, size.x, size.y);
    for (var i = 0; i < this.pools.length; i++){
      var pool = this.pools[i];
      this.renderPool(pool, loc, size);
    }
    for (var j = 0; j < this.avatars.length; j++){
      var avatar = this.avatars[j];
      this.renderAvatar(avatar);
    }    
  }
  this.renderPool = (pool, loc, size) => {
    ctx.fillStyle="#ffa1";
    ctx.strokeStyle="#ffa3";
    var b = 15*scale;
    var w = size.x - 2*b;
    var h = size.y - 2*b;
    roundRect(ctx, loc.x+b+w*pool.loc.x, loc.y+b+h*pool.loc.y, w*pool.size.x, h*pool.size.y, 20, true, true);
  }
  this.renderAvatar = (avatar) => {
    
  }
  
  this.updateViewIndex = () => {
  	if (Math.abs(this.moveDistance)>30){
  		if (this.moveDistance>0){
  			this.currentViewIndex++;
  			if (this.currentViewIndex == 3)
  				this.currentViewIndex = 0;
  		} else {
	  		this.currentViewIndex--;
  			if (this.currentViewIndex == -1)
  				this.currentViewIndex = 2;
  		}
  		this.moveDistance -= this.moveSpeed;
  		this.moveDistance *= -1;
  		
  	}
  }
  
  this.clicked = (sender, loc) => {
   
  }
  this.released = () => { 
  	
  }
  this.moved = (loc, delta) => {   	
  	if(loc.y < canvas.height/3){
  		this.moveDistance += delta.x;
			this.moveSpeed = delta.x;
			this.updateViewIndex();
  	}
  }
}
